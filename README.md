# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣


## HTML & CSS Task

## Objective
* Checkout the dev branch.
* Create a checkout form which includes the personal details of the customer.
* When implemented merge the dev branch to master.
## Requirements
* Start the project with **npm run start**.
* The form must contain an input type email with the name "email" with an **input** class
* The form must contain an input type text with the name "name" with an **input** class
* The form must contain an input type text with the name "address" with an **input** class
* The form must contain an input type tel with the name "phone" with an **input** class
* The form must contain an input type color with the name "color" with an **input** class
* The form must contain an input type date with the name "delivery_date" with an **input** class
